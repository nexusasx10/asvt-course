model tiny
.code
locals
org 100h

_main:
xor cx, cx
mov cl, es:[80h]
mov si, 82h

read_arg:
dec cx
inc si
xor dx, dx
mov dl, es:[si]
cmp dl, 20h
je skip
cmp dl, 09h
je skip
cmp dl, 2Dh
je define
jmp skip
define:
cmp word ptr es:[si], 4C41h
je no
cmp word ptr es:[si], 6C61h
je no
xor ax, ax
no:
skip:
cmp cx, 1
jnz read_arg
ret
end _main
