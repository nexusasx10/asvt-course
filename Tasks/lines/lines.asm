model tiny
.code
org 100h
locals
.486

_main:
; save mode
mov ah, 0Fh
int 10h
push ax

; set mode 3
mov ah, 0
mov al, 03h
int 10h

; get resolution
xor ax, ax
mov ah, 15h
int 10h
add di, 6
mov ax, word ptr es:di

; move cursor
mov ah, 02h
mov bh, 0
mov dh, 5
mov dl, 5
int 10h

; out character
mov ah, 09h
mov al, 50
mov bh, 0
mov bl, 00000110b
mov cx, 1
int 10h

; wait for character
mov ah, 00
int 16h

; restore mode
pop ax
mov ah, 0
int 10h

mov ah, 4ch
int 21h
ret
end _main
