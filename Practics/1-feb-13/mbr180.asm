.model tiny
.code
org 100h
begin:
jmp short _start
nop
operator db 'MBR.180K'
; BPB
BytesPerSec dw 200h
SectPerClust db 1
RsvdSectors dw 1
NumFATs db 2
RootEntryCnt dw 64
TotalSectros dw 360
MediaByte db 0FCh
FATsize dw 2
SecPerTrk dw 9
NumHeads dw 1
HidSec dw 0, 0
TotSec32 dd 0
DrvNum db 0
Reserved db 0
Signatura db ')' ; 29h
;
Vol_ID db 'XDRV'
DiskLabel db 'TestMBRdisk'
FATtype db 'FAT12   '
; B800:0000 - videomem, symbol - 2B, AttrSym,AttrBack,Char,Char
_start:
cld

; int 16 f 0 - прочитать символ (в AL)
; 0E - backspace, 01 - Esc


mov ax, 0B800h
mov es, ax
mov di, 0
mov ax, 410Ah
stosw

xor ax, ax
int 16h
int 19h
org 766
dw 0aa55h
end begin
