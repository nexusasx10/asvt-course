model tiny
.code
org 100h

_start:
xor ax, ax
mov ah, 02h
xor dx, dx
mov cx, 255
_met:
int 21h
inc dx
dec cx
cmp cx, 0
jne _met
ret
end _start
