model tiny
.code
org 100h

_main:
xor ax, ax
mov ah, 0Ah
lea dx, buffer
int 21h

xor bx, bx
mov bl, byte ptr buffer + 1
lea dx, buffer
add bx, dx
add bx, 2
mov byte ptr [bx], 24h

xor ax, ax
xor dx, dx
mov ah, 02h
mov dl, 0Ah
int 21h
mov dl, 0Dh
int 21h

xor ax, ax
mov ah, 09h
lea dx, buffer
add dx, 2
int 21h
ret
buffer db 6, 0, 6 dup(0)
end _main
